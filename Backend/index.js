require('nodetime').profile({
    accountKey: '6a594f360fdcb388e8176455d1472d1501da3d97', 
    appName: 'Node.js Application'
});

var server = require("./server");
var router = require("./router");
var requestHandlers = require("./requestHandlers");

var handle = {}
handle["/setAppointment"] = requestHandlers.setAppointment;
handle["/getAppointments"] = requestHandlers.getAppointments;
handle["/registerUser"] = requestHandlers.registerUser;
handle["/authentication"] = requestHandlers.authentication;
handle["/getPatientAppointments"] = requestHandlers.getPatientAppointments;
handle["/deleteAppointment"] = requestHandlers.deleteAppointment;
handle["/checkAppointment"] = requestHandlers.checkAppointment;

server.start(router.route, handle);
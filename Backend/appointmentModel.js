var mongoose = require('mongoose');

var appointmentSchema = new mongoose.Schema({
			month: String,
			day: String,
			year: String,
			hour: String,
			minute: String,
			doctor: String,
			reason: String,
			patientId: String
		});

module.exports = mongoose.model('appointments', appointmentSchema);
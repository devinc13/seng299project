var exec = require("child_process").exec;
var db = require('./db.js');
var Appointments = require('./appointmentModel.js');
var Users = require('./userModel.js');
var url = require('url');
var querystring = require('querystring');

function deleteAppointment(request, response) {
	db.on('error',console.error);
	var queryAsObject = url.parse(request.url,true).query;
	var queryWithoutCallback = {}
	queryWithoutCallback._id = queryAsObject._id.toString();
	Appointments.remove(queryWithoutCallback,function(err, appointment) {
		if (err) return console.error(err);
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.write("callback(" + JSON.stringify(appointment, undefined, 2) + ");");
		response.end();
	});
}

function getAppointments(request, response) {
	setTimeout(function(){ 
		console.log("waiting");
		db.on('error',console.error);
		var queryAsObject = url.parse(request.url,true).query;
		var queryWithoutCallback =  {}

		if(typeof queryAsObject.patientId != "undefined")
			queryWithoutCallback.patientId = queryAsObject.patientId.toString();
		if(typeof queryAsObject.doctor != "undefined")
			queryWithoutCallback.doctor = queryAsObject.doctor.toString();
		if(typeof queryAsObject.year != "undefined")
			queryWithoutCallback.year = queryAsObject.year.toString();
		if(typeof queryAsObject.month != "undefined")
			queryWithoutCallback.month = queryAsObject.month.toString();
		if(typeof queryAsObject._id != "undefined")
			queryWithoutCallback._id = queryAsObject._id.toString();
			

		Appointments.find(queryWithoutCallback,function(err, appointment) {
			if (err) return console.error(err);
			response.writeHead(200, {"Content-Type": "text/plain"});
			var appointments = {'appointments' : appointment}
			var available = []
			if((typeof queryAsObject.available != "undefined")){
				for(i = 0; i < appointments.appointments.length; i++){
					var appointment = {}
					appointment.hour = appointments.appointments[i].hour
					appointment.minute = appointments.appointments[i].minute
					appointment.day = appointments.appointments[i].day
					available.push(appointment);
				}
				response.write("callback(" + JSON.stringify(available, undefined, 2) + ");");
				response.end();
			}else{
				response.write("callback(" + JSON.stringify(appointments, undefined, 2) + ");");
				response.end();
			}
		}); 
	}, 2200);
}

function checkAppointment(request, response) {
	setTimeout(function(){ 
		console.log("waiting");
		db.on('error',console.error);
		var queryAsObject = url.parse(request.url,true).query;
		var queryWithoutCallback =  {}

		if(typeof queryAsObject.patientId != "undefined")
			queryWithoutCallback.patientId = queryAsObject.patientId.toString();
		if(typeof queryAsObject.doctor != "undefined")
			queryWithoutCallback.doctor = queryAsObject.doctor.toString();
		if(typeof queryAsObject.year != "undefined")
			queryWithoutCallback.year = queryAsObject.year.toString();
		if(typeof queryAsObject.month != "undefined")
			queryWithoutCallback.month = queryAsObject.month.toString();
		if(typeof queryAsObject.day != "undefined")
			queryWithoutCallback.day = queryAsObject.day.toString();
		if(typeof queryAsObject.hour != "undefined")
			queryWithoutCallback.hour = queryAsObject.hour.toString();
		if(typeof queryAsObject.minute != "undefined")
			queryWithoutCallback.minute = queryAsObject.minute.toString();
		if(typeof queryAsObject._id != "undefined")
			queryWithoutCallback._id = queryAsObject._id.toString();
		
		console.log(JSON.stringify(queryWithoutCallback));	

		Appointments.find(queryWithoutCallback,function(err, appointment) {
			if (err) return console.error(err);
			response.writeHead(200, {"Content-Type": "text/plain"});
			var appointments = {'appointments' : appointment}
			var available = []
			if((typeof queryAsObject.available != "undefined")){
				for(i = 0; i < appointments.appointments.length; i++){
					var appointment = {}
					appointment.hour = appointments.appointments[i].hour
					appointment.minute = appointments.appointments[i].minute
					appointment.day = appointments.appointments[i].day
					available.push(appointment);
				}
				response.write("callback(" + JSON.stringify(available, undefined, 2) + ");");
				response.end();
			}else{
				response.write("callback(" + JSON.stringify(appointments, undefined, 2) + ");");
				response.end();
			}
		}); 
	}, 0);
}



// request should look like this : localhost:8888/getPatientAppointments?patientId=53ae099765b2a5470c8d94f6
function getPatientAppointments(request, response) {
	db.on('error',console.error);
	var queryAsObject = url.parse(request.url,true).query;
	var queryWithoutCallback =  {}
	queryWithoutCallback.patientId = queryAsObject.patientId.toString();
		
	Appointments.find(queryWithoutCallback,function(err, appointment) {
		if (err) return console.error(err);
		response.writeHead(200, {"Content-Type": "text/plain"});
		var appointments = {'appointments' : appointment}
		var appointmentsArray = []

		for(i = 0; i < appointments.appointments.length; i++){
			var appointment = appointments.appointments[i]
			appointmentsArray.push(appointment);
		}
		response.write("callback(" + JSON.stringify(appointmentsArray, undefined, 2) + ");");
		response.end();
	});
}

function authentication(request, response) {
	db.on('error',console.error);
	var queryAsObject = url.parse(request.url,true).query;
	var queryWithoutCallback =  {
		email: queryAsObject.email.toString(),
		password: queryAsObject.password.toString()
	}
	Users.findOne(queryWithoutCallback,function(err, result){
		if (err) return console.error(err);
		if (result === null) {
			response.writeHead(200, {"Content-Type": "text/plain"});
			response.write("callback(" + "null" + ");");
			response.end();
		} else {
			response.writeHead(200, {"Content-Type": "text/plain"});
			response.write("callback(" + JSON.stringify(result, undefined, 2) + ");");
			response.end();
		}
	});
}

// REQUEST SHOULD LOOK LIKE THIS:
//localhost:8888/setAppointment?month=02&day=13&year=2015&hour=12&minute=00&doctor=Doctor+Joe&reason=My+foot+hurts.&patientId=53ae099765b2a5470c8d94f6
//NOTE: Spaces need to be replaced with the + sign
//NOTE: Date is split up into month, day, year, hour and minute
function setAppointment(request, response) {
	db.on('error',console.error);
	var queryAsObject = new Appointments(url.parse(request.url,true).query);
	var queryWithoutCallback =  new Appointments({
		month: queryAsObject.month.toString(),
		day: queryAsObject.day.toString(),
		year: queryAsObject.year.toString(),
		hour: queryAsObject.hour.toString(),
		minute: queryAsObject.minute.toString(),
		doctor: queryAsObject.doctor.toString(),
		reason: queryAsObject.reason.toString(),
		patientId: queryAsObject.patientId.toString()
	});
	queryWithoutCallback.save(function(err,appointment) {
		if (err) return console.error(err);
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.write("callback(" + JSON.stringify(appointment, undefined, 2) + ");");
		response.end();
	});
}

// REQUEST SHOULD LOOK LIKE THIS:
//localhost:8888/registerUser?email=testUser@gmail.com&password=password&firstName=test&lastName=user&phoneNumber=2500000000
function registerUser(request, response) {
	db.on('error',console.error);
	var queryAsObject = new Users(url.parse(request.url,true).query);
	queryAsObject.save(function(err,user) {
		if (err) return console.error(err);
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.write("callback(" + JSON.stringify(user, undefined, 2) + ");");
		response.end();
	});
}


exports.checkAppointment = checkAppointment;
exports.getAppointments = getAppointments;
exports.setAppointment = setAppointment;
exports.registerUser = registerUser;
exports.authentication = authentication;
exports.getPatientAppointments = getPatientAppointments;
exports.deleteAppointment = deleteAppointment;
